Essays
======

Here are the mathematical essays I produced as an undergraduate for both the
Rouse Ball Prize and my Part III dissertation. The topics are as follows:

 - Statistical Applications of Persistent Homology (2016, Part III essay);
 - Octonions and G2 manifolds (2015);
 - Quantum computation (2014);
 - Euclidean Ramsey theory (2013).
